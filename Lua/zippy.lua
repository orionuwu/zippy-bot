-- import libraries
local discordia = require('discordia')
local json = require('json')
local random = require('math').random

-- open and parse Config.JSON
local monkeFile = io.open('./config.json', 'r')
local monkeData = json.decode(monkeFile:read('*all'))
monkeFile:close()

-- define variables, these let me access the "list" (array) with the images of my apes and monkeys
local monkeImages = monkeData['monkeImages']
local monkeToken = monkeData['token']
local monkeTrigger = "zippy"

-- create the bot client
local client = discordia.Client()

-- Bot logic, it tests if messages were sent by bots, then if they contain the word "zippy"
-- if both pass then it randomly selects an entry in my array and then sends it
client:on('messageCreate', function(message)
    if message.author.bot or not message.content then
        return
    end

    local monkeMessages = string.lower(message.content)

    if string.find(monkeMessages, monkeTrigger) then
        local randMonke = monkeImages[random(#monkeImages)]
        print(monkeMessages)
        message.channel:send(randMonke)
    end
end)

-- login the bot
client:run('Bot ' .. monkeToken)
