const Eris = require("eris"); // Discord Bot Library
const { token, monkeImages } = require('./config.json');

// Verify with Discord
const bot = new Eris(token, {
    intents: [
        "guildMessages"
    ]
});

// Log "Monke Ready!" once the bot is ready
bot.on("ready", () => {
    console.log("Monke Ready!");
});

// Send a random monkey image when the message contains a specific word
bot.on("messageCreate", (msg) => {
    const monkeTrigger = msg.content.toUpperCase();

    monkeWord("ZIPPY");

    function monkeWord(monke) {
        if (monkeTrigger.includes(monke)) {
            const randomMonke = monkeImages[Math.floor(Math.random() * monkeImages.length)];
            bot.createMessage(msg.channel.id, randomMonke);
        }
    }
});

// Get the bot to connect to Discord
bot.connect();
