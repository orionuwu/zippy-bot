# import goofy ahh libraries
import json
import random
import hikari

# Open and parse Config.JSON
monkeFile = open('config.json')
monkeData = json.load(monkeFile)

# Define variables, these let me access the "list" (array) with the images of my apes and monkeys
monkeImages = monkeData['monkeImages']
monkeToken = monkeData['token']
monkeTrigger = "zippy"


# init the gotdamn bot
bot = hikari.GatewayBot(monkeToken, intents=hikari.Intents.ALL_UNPRIVILEGED | hikari.Intents.MESSAGE_CONTENT)


# Bot logic, it tests if messages were sent by bots, then if they contain the word "zippy"
# if both pass then it randomly selects an entry in my array and then sends it
@bot.listen()
async def ping(event: hikari.MessageCreateEvent) -> None:
    if event.is_bot or not event.content:
       return

    monkeMessages = event.content.lower()

    if monkeTrigger in monkeMessages:
        randMonke = random.choice(monkeImages)
        print(monkeMessages)
        await event.message.respond(randMonke)

bot.run()
